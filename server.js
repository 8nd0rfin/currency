/**
 * Локальный сервер.
 * Для проверки локальной сборки.
 * Его же можно использовать в качестве API сервера.
 */
const express = require('express')
const app = express()
const port = 4444


app.use(express.static(__dirname + "/dist"))

app.get('/',(req,res) => {
    res.sendFile('dist/index.html')
})

app.listen(port,() => {
    console.log(`Server running at ${port} port...`)
})
