import Api from "@/plugins/classes/Api.class";
/**
 * Плагин для работы по протоколу HTTP.
 * Для данной задачи выбран интерфейс API XMLHttpRequest. И вынесен из контекста NUXT.
 * Можно использовать fetch, axios.
 * Также можно реализовать через vuex dispatch.
 */
export default function (ctx,inject) {
    if(typeof window === 'undefined') return
    window['$api'] = new Api()
}
