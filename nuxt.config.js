module.exports = {
    head: {
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'shortcut icon', type: 'image/png', sizes:'16x16', href: '/favicon-16x16.png'},
            {rel: 'shortcut icon', type: 'image/png', sizes:'32x32', href: '/favicon-32x32.png'},
            {rel: 'apple-touch-icon', type: 'image/png', href: '/apple-touch-icon'},
            {rel: 'apple-touch-icon', type: 'image/png', sizes:'192x192', href: '/android-chrome-192x192'},
            {rel: 'apple-touch-icon', type: 'image/png', sizes:'512x512', href: '/android-chrome-512x512'},
            {rel: 'preconnect', href: 'https://fonts.googleapis.com'},
            {rel: 'preconnect', href: 'https://fonts.gstatic.com',crossOrigin:true},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;900&display=swap'}
        ],
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover'},
            {name:'apple-mobile-web-app-status-bar-style',content:'#303b44'},
            {name:'apple-mobile-web-app-capable',content:'yes'},
            {name:'mobile-web-app-capable',content:'yes'}
        ]
    },
    css:[
        {src:'~/assets/scss/base.scss',lang:'scss'},
        {src:'~/assets/scss/colors.scss',lang:'scss'}
    ],
    plugins: ['~/plugins/Api.js']
}
