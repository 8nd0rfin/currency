const getters = {
    from(state) {
        return state.from
    },
    to(state) {
        return state.to
    },
    currencyPairs(state) {
        return state.currencyPairs
    },
    exchangeRates(state) {
        return state.exchangeRates
    }
}
export default getters
