const state = () => ({
    from:'RUB',
    to:'USD',
    currencyPairs:[],
    exchangeRates:[],
    convertationComission:'0'
})
export default state
