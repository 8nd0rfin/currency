const actions = {
    /**
     * Запрашивает от API массив конвертируемых валют.
     * Полученный массив возвращает и сохраняет в state
     * @param context
     * @return {Promise<Response<array>>}
     */
    async setCurrencyPairs(context) {
        if (typeof window === 'undefined') return false
        let currencyPairs = await window.$api.request({path:'getCurrencyPairs'})
        context.commit('setCurrencyPairs',currencyPairs)
        return currencyPairs
    },
    /**
     * Запрашивает от API массив конвертируемых валют c коэффициентом конвертации относительно основной валюты.
     * Полученный массив возвращает и сохраняет в state
     * @param context
     * @return {Promise<Response<array>>}
     */
    async setExchangeRates(context) {
        if (typeof window === 'undefined') return false
        let exchangeRates = await window.$api.request({path:'getExchangeRates'})
        context.commit('setExchangeRates',exchangeRates)
        return exchangeRates
    },
    /**
     * Рассчитывает % комиссии
     * @param context
     * @return {Promise<string>}
     */
    async getComission(context) {
        let pairFrom,
            pairTo
        const currencyPairs = context.state.currencyPairs
        currencyPairs.forEach(p => {
            if (p.quote_currency === context.state.from) {
                pairFrom = p
            }
            if (p.quote_currency === context.state.to) {
                pairTo = p
            }
        })
        const commission = pairFrom.commission + pairTo.commission
        context.state.convertationComission = commission.toString()
        return context.state.convertationComission
    }
}
export default actions
