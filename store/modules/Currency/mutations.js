const mutations = {
    setFrom(state,data) {
        state.from = data
    },
    setTo(state,data) {
        state.to = data
    },
    setCurrencyPairs(state,data) {
        state.currencyPairs = data
    },
    setExchangeRates(state,data) {
        state.exchangeRates = data
    }
}
export default mutations
