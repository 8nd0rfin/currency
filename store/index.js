import Vuex from 'vuex'

import state from './state.js'
import getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions.js'

import currency from './modules/Currency/index'

const createStore = () => {
    return new Vuex.Store({
        state,
        getters,
        mutations,
        actions,
        modules: {
            currency,
        }
    })
};

export default createStore
