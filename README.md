# Конвертер валют на NUXT.js

:ru: by Igor Bludov

Тестовое задание.  
Приложение реализовано с помощью [NUXT.js](https://nuxtjs.org/)
и представляет собой простейший конвертер валют, демонстрируя также концепции
[SPA](https://ru.wikipedia.org/wiki/%D0%9E%D0%B4%D0%BD%D0%BE%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B5_%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5) приложения.
[Тестовое задание](https://gitlab.com/8nd0rfin/currency/-/blob/master/Test_Assignment_-_Frontend_(SPA).docx)

## [Конвертер на VUE 3](https://gitlab.com/8nd0rfin/currency-on-vue-3-with-ts)
Этот же конвертер реализованный на [VUE 3](https://v3.ru.vuejs.org/ru/guide/composition-api-introduction.html) с использованием [composition API](https://v3.ru.vuejs.org/ru/) и TypeScript. Так же несколько оптимизировал некоторые объекты и методы.

 

:triangular_ruler: [Общие принципы работы приложения](#architecture)

:hammer: [Список использованных технологий и инструментов](#tools)

:wrench: [Локальный запуск проекта](#dev)


## <a name="architecture"></a>Общие принципы работы приложения

В приложении используется [Vuex](https://vuex.vuejs.org/) в качестве централизованного хранилища данных
для всех компонентов. 

Для ускорения процесса разработки использован HTML-шаблонизатор [Pug](https://pugjs.org/api/getting-started.html),
CSS препроцессор [Scss](https://sass-lang.com/).

В проекте добавлен файл server.js. Этот файл запускает локальный сервер http://localhost:4444 на котором можно проверить
работу сгенерированного конвертера.


## <a name="tools"></a>Список использованных технологий и инструментов

- [Vue](https://vuejs.org/)
- [Vue Router](https://router.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Pug](https://pugjs.org/api/getting-started.html)
- [Sсss](https://sass-lang.com/documentation/syntax)
- [Webpack](https://webpack.js.org/)

## <a name="dev"></a>Локальный запуск проекта

1. **Загрузка проекта на локальную машину**

    ```
    git clone https://gitlab.com/8nd0rfin/currency.git
    ```

2. **Установка зависимостей**

    ```
    npm ci
    ```

   Использование `npm ci` вместо `npm i` позволит гарантировать корректные версии устанавливаемых `npm` пакетов,
   так как они будут взяты из `package-lock.json`.

3. **Запуск сервера для разработки**

   Для его запуска выполните из корневой директории проекта:

    ```
    npm run dev
   ```

   После этого приложение будет доступно по адресу `localhost:3000`.

   Также доступны следующие команды:

    ```
    npm run generate // генерация страниц для production версии
    ```

4. **Запуск локального сервера для проверки production версии**

    Из корневой директории проекта:
    
    ```
    node server.js // запуск локального сервера http://localhost:5555
    ```
