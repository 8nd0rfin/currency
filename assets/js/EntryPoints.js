function getEntryPoint(entryPoint) {
    let result = ''
    try {
        if (process.env['VUE_ENV'] == 'server') {
            if (typeof (process.env['STAGING']) != 'undefined' && process.env['STAGING'] == 1) {
                result = entryPoints[entryPoint].staging
            }
            else if (typeof (process.env['CHAOS']) != 'undefined' && process.env['CHAOS'] == 1) {
                result = entryPoints[entryPoint].chaos
            }
            else if (typeof (process.env['PRODUCTION']) != 'undefined' && process.env['PRODUCTION'] == 1) {
                result = entryPoints[entryPoint].production
            }
            else {
                result = entryPoints[entryPoint].chaos
            }
        } else {
            if (window.location.hostname == 'begeton.com' || window.location.hostname == 'www0.begeton.com' || window.location.hostname == 'new.begeton.com') {
                result = entryPoints[entryPoint].production
            }
            else if (window.location.hostname == 'chaos.begeton.com') {
                result = entryPoints[entryPoint].chaos
            }
            else if (window.location.hostname.indexOf('staging.begeton') > -1) {
                result = entryPoints[entryPoint].staging
            }
            else if (window.location.hostname.indexOf('192.168') > -1 || window.location.hostname.indexOf('172.') > -1) {
                result = entryPoints[entryPoint].chaos
            }
            else {
                result = entryPoints[entryPoint].chaos
            }
        }
    } catch (e) {
        console.error('Error getEntryPoint: ',e)
    }

    return result
}
function randomX() {
    let max = 9
    let min = 0

    let rndm = Math.floor(Math.random() * (max - min + 1) ) + min
// webSocketUrl = 'ws.begeton.com'

    // Временный фикс при смене дивергенции серваков
    // rndm = rndm === 3 ? 4 : rndm

    return rndm
}
const entryPoints = {
    webSocket: {
        staging: 'ws-staging.begeton.com',
        chaos: 'ws-chaos.begeton.com',
        production: 'ws.begeton.com',
    },



    api: {
        chaos: {
            key:'f06ef6883253e9960881970c584bc6ad',
            url:function(){
                let result = "https://api-chaos0" + randomX() + ".begeton.com/" // Default - for client side
                if (process.env['VUE_ENV'] == 'server') {
                    result = "https://api-chaos.begeton.com/"
                }
                if(!result) {console.error('Error in entry points: result is null')}
                return result
            },
            fileApiUrl:'https://f-chaos.begeton.com/'
        },
        staging: {
            key:'f06ef6883253e9960881970c584bc6ad',
            url:function(){
                let result = "https://api-staging0" + randomX() + ".begeton.com/" // Default - for client side
                if (process.env['VUE_ENV'] == 'server') {
                    result = "https://api-staging.begeton.com/"
                }
                if(!result) {console.error('Error in entry points: result is null')}
                return result
            },
            fileApiUrl:'https://f-staging.begeton.com/'
        },
        production: {
            key: 'ee5f8642f7bda3b796f73426093fd526',
            url: function () {
                let result = "https://api0" + randomX() + ".begeton.com/" // Default - for client side
                if (process.env['VUE_ENV'] == 'server') {
                    result = "https://api0.begeton.com/"
                }
                if(!result) {console.error('Error in entry points: result is null')}
                return result
            },
            fileApiUrl:'https://f.begeton.com/'
        }
    },


    apiPhp: {
        chaos: {
            url:'https://chaos.begeton.com/api/api.php',
            isPrintLog:false,
        },
        staging: {
            url:'https://staging.begeton.com/api/api.php',
            isPrintLog:false,
        },
        production: {
            url:'https://begeton.com/api/api.php',
            isPrintLog:false,
        },
    },


    captcha: {
        chaos: {
            url:'https://captcha-chaos.begeton.com/captcha2/',
        },
        staging: {
            url:'https://captcha-staging.begeton.com/captcha2/',
        },
        production: {
            url:'https://captcha.begeton.com/captcha2/',
        },
    },

    // ID службы оформления заказов (для отправки сообщений)
    orderService: {
        chaos: {
            id:55950,
        },
        staging: {
            id:55950,
        },
        production: {
            id:58471,
        },
    },

    domain: {
        chaos: {
            url:'https://chaos.begeton.com',
        },
        staging: {
            url:'https://staging.begeton.com',
        },
        production: {
            url:'https://begeton.com',
        },
    },
    begeton: {
        chaos: {
            userId: 4,
        },
        staging: {
            userId: 4,
        },
        production: {
            userId: 41958,
        },
    },
    partnersList: {
        chaos: [
            {
                id: 55213,
                title: 'Jobsora',
                logo: '/img/brands_logo/jobsora-logo.png',
                link:'//ru.jobsora.com/'
            },
            {
                id: 55205,
                title: 'Jooble',
                logo: '/img/brands_logo/jooble-logo.svg',
                link:'//ru.jooble.org/'
            },
            {
                id: 58402,
                title: 'Jobeka',
                logo: '/files/users-companies_thumb/120/6/31/InqaQyUtTPeT3eG5LCUjwpj0cXhkDmuS.jpg.png',
                link:'//jobeka.com/'
            },
        ],
        staging: [
            {
                id: 55213,
                title: 'Jobsora',
                logo: '/img/brands_logo/jobsora-logo.png',
                link:'//ru.jobsora.com/'
            },
            {
                id: 55205,
                title: 'Jooble',
                logo: '/img/brands_logo/jooble-logo.svg',
                link:'//ru.jooble.org/'
            },
            {
                id: 58402,
                title: 'Jobeka',
                logo: '/files/users-companies_thumb/120/6/31/InqaQyUtTPeT3eG5LCUjwpj0cXhkDmuS.jpg.png',
                link:'//jobeka.com/'
            },
        ],
        production: [
            {
                id: 55213,
                title: 'Jobsora',
                logo: '/img/brands_logo/jobsora-logo.png',
                link:'//ru.jobsora.com/'
            },
            {
                id: 55205,
                title: 'Jooble',
                logo: '/img/brands_logo/jooble-logo.svg',
                link:'//ru.jooble.org/'
            },
            {
                id: 58402,
                title: 'Jobeka',
                logo: '/files/users-companies_thumb/120/6/31/InqaQyUtTPeT3eG5LCUjwpj0cXhkDmuS.jpg.png',
                link:'//jobeka.com/'
            },
        ],
    },
    partnerAccount: {
        chaos: {
            userId: 53247,
        },
        staging: {
            userId: 53247,
        },
        production: {
            userId: 53247,
        },
    },
    stat: {
        chaos: {
            url:'https://stat-chaos.begeton.com',
        },
        staging: {
            url:'https://stat-staging.begeton.com',
        },
        production: {
            url:'https://stat.begeton.com',
        },
    },

    networking:{
        chaos: {
            id: 1368,
        },
        staging: {
            id: 1368,
        },
        production: {
            id: 1368
        },
    },

    adminServices:{
        chaos: {
            url: 'http://internal-api-chaos.backstage.local',
        },
        staging: {
            url: 'http://internal-api-staging.backstage.local',
        },
        production: {
            url: 'http://internal-api.backstage.local'
        },
    }

}


const webSocketProtocol = 'wss'
const webSocketPort = '443'
let webSocketUrl = getEntryPoint('webSocket')




export const api = getEntryPoint('api')
export const apiPhp = getEntryPoint('apiPhp')
export const captcha = getEntryPoint('captcha')
export const domain = getEntryPoint('domain')
export const begeton = getEntryPoint('begeton')
export const partnersList = getEntryPoint('partnersList')
export const partnerAccount = getEntryPoint('partnerAccount')
export const stat = getEntryPoint('stat')
export const orderService = getEntryPoint('orderService')
export const adminServices = getEntryPoint('adminServices')




// TODO-done: Made entry points auto detect staging and production fo all entrypoints: api, apiphp, ws and other

export default {
    webSocket: {
        getWebSocketPathBySID: function(sid) {
            if(!sid) {
                console.error('Error in webSocket entry points: SID must be defined.')
                return false
            }
            return webSocketProtocol
                    + '://' + webSocketUrl
                    + ':' + webSocketPort
                    + '/?session=' + sid
        },
        getWebSocketPathByUserPass: function(user,password) {
            if(!user || !password) {
                console.error('Error in webSocket entry points: user or password must be defined.')
                return ''
            }
            return webSocketProtocol
                    + '://' + webSocketUrl
                    + ':' + webSocketPort
                    + '/?user=' + user
                    + '&password=' + password
        }
    }
}
