import {randomize} from "@/assets/js/utils";

/**
 * Основная валюта, относительно которой рассчитывается коэффициент конвертации для остальных валют
 * @type {string}
 */
let base_currency = 'RUB'
/**
 * Список валют для конвертации
 */
const CurrencyList = {
    RUB:{
        "color": "/img/colors/RU.svg",
        "name": "Российский рубль",
        "nominal": 1
    },
    USD:{
        "color": "/img/colors/US.svg",
        "name": "Доллар США",
        "nominal": 1
    },
    EUR:{
        "color": "/img/colors/EU.svg",
        "name": "Евро",
        "nominal": 1
    },
    AUD:{
        "color": "/img/colors/AU.svg",
        "name": "Австралийский доллар",
        "nominal": 1
    },
    GBP:{
        "color": "/img/colors/UK.svg",
        "name": "Фунт стерлингов Соединенного королевства",
        "nominal": 1
    },
    BYN:{
        "color": "/img/colors/BY.svg",
        "name": "Белорусский рубль",
        "nominal": 1
    },
    INR:{
        "color": "/img/colors/IN.svg",
        "name": "Индийская рупия",
        "nominal": 100
    },
    CAD:{
        "color": "/img/colors/CA.svg",
        "name": "Канадский доллар",
        "nominal": 1
    },
    CNY:{
        "color": "/img/colors/CN.svg",
        "name": "Китайский юань",
        "nominal": 10
    },
    JPY:{
        "color": "/img/colors/JP.svg",
        "name": "Японская иена",
        "nominal": 100
    }
}
/**
 * Массив комиссий
 * @type {number[]}
 */
const Commissions = [1.00,2.00,3.00,4.00,5.00]

export function getCurrencyPairs() {
    return Object.keys(CurrencyList).reduce((arr,currency) => {
        arr.push({
            color:CurrencyList[currency]['color'],
            base_currency,
            quote_currency:currency,
            commission: Commissions[randomize(0,4)]
        })
        return arr
    },[])
}

export function getExchangeRates() {
    return Object.keys(CurrencyList).reduce((arr,currency) => {
        arr.push({
            color:CurrencyList[currency]['color'],
            pair:`${base_currency}/${currency}`,
            rate: currency === base_currency ? 1 : `0.${randomize(0,100)}`
        })
        return arr
    },[])
}
