/**
 * Возвращает тип объекта в текстовом виде ('String','Number','Array','Object')
 * @param elm {Number/Array/String/Object/undefined}
 * @return {string}
 */
export function getType(elm) {
    try {
        const o = {}
        return o.toString.call(elm).replace('object ', '').replace(/\[*\]*/gi, '')
    } catch (e) {
        console.error('Utils.js.getType error: ',e)
    }
}
/**
 * Для обращения к значениям вложенных элементов объекта
 * исключая неожиданные ошибки UNDEFINED
 * Пример: получение значения в объекте на третьем уровне
 * getNestedObject(user, ['personalInfo', 'address','city']);
 * аналогично user.personalInfo.address.city
 * @param nestedObj
 * @param pathArr
 * @return {*}
 */
export function getNestedObject(nestedObj, pathArr) {
    try {
        return pathArr.reduce((obj, key) =>
            (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);
    } catch (e) {
        console.error('Utils:getNestedObject  error: ',e)
    }
}
/**
 * Функция "заслонки/замедления" запросов, действий, событий
 * @param func - замедляемая функция
 * @param wait {Number} - число в миллисекундах, время задержки
 * @param options {Object} - объект с дополнительными параметрами
 * @return {*}
 */
export function throttle (func, wait, options) {
    let _ = {
        now: Date.now || function() {
            return new Date().getTime();
        }
    };
    let context, args, result;
    let timeout = null;
    let previous = 0;
    if (!options) {
        options = {};
    }
    const later = function() {
        previous = options.leading === false ? 0 : _.now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) {
            context = args = null;
        }
    };

    let returned = function () {
        let now = _.now();
        if (!previous && options.leading === false) {
            previous = now;
        }
        let remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        }
        else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    }
    return returned()
}
/**
 * Случайное число в указанном диапазоне включая min, max
 * @param min {Number} - минимальное число в диапазоне
 * @param max {Number} - максимальное число в диапазоне
 * @return {number}
 */
export const randomize = function(min = 0,max = 10) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}
/**
 * Обёртка. Запустит функцию через delay после поcледнего вызова
 */
let timeout;
export function debounce(callback, delay = 500) {
    return function(...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            callback(args);
        }, delay);
    };
}

